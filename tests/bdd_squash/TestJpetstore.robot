*** Settings ***
Resource    squash_resources.resource


*** Variables ***
${user}        j2ee
${pwd}         j2ee
${message}     Welcome ABC!

*** Keywords ***


*** Test Cases ***
ConnexionJpetstore
    [Setup]    Acces page d'accueil
    Given Je suis sur la page d'accueil de Jpetstore
    When je clique sur le lien de connexion
    And rentre le Username "${user}"
    And rentre le Password "${pwd}"
    And je clique sur login
    Then utilisateur ABC est connecte
    And je peux lire le message accueil "${message}"
    [Teardown]    Sortir de l'application